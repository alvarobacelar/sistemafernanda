json.extract! promissoria, :id, :descricao, :data_vencimento, :valor_debito, :created_at, :updated_at
json.url promissoria_url(promissoria, format: :json)