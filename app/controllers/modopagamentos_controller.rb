class ModopagamentosController < ApplicationController
  before_action :set_modopagamento, only: [:show, :edit, :update, :destroy]

  # GET /modopagamentos
  # GET /modopagamentos.json
  def index
    @modopagamentos = Modopagamento.all
  end

  # GET /modopagamentos/1
  # GET /modopagamentos/1.json
  def show
  end

  # GET /modopagamentos/new
  def new
    @modopagamento = Modopagamento.new
  end

  # GET /modopagamentos/1/edit
  def edit
  end

  # POST /modopagamentos
  # POST /modopagamentos.json
  def create
    @modopagamento = Modopagamento.new(modopagamento_params)

    respond_to do |format|
      if @modopagamento.save
        format.html { redirect_to @modopagamento, notice: 'Modopagamento was successfully created.' }
        format.json { render :show, status: :created, location: @modopagamento }
      else
        format.html { render :new }
        format.json { render json: @modopagamento.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /modopagamentos/1
  # PATCH/PUT /modopagamentos/1.json
  def update
    respond_to do |format|
      if @modopagamento.update(modopagamento_params)
        format.html { redirect_to @modopagamento, notice: 'Modopagamento was successfully updated.' }
        format.json { render :show, status: :ok, location: @modopagamento }
      else
        format.html { render :edit }
        format.json { render json: @modopagamento.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /modopagamentos/1
  # DELETE /modopagamentos/1.json
  def destroy
    @modopagamento.destroy
    respond_to do |format|
      format.html { redirect_to modopagamentos_url, notice: 'Modopagamento was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_modopagamento
      @modopagamento = Modopagamento.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def modopagamento_params
      params.require(:modopagamento).permit(:tipo, :porcentagem)
    end
end
