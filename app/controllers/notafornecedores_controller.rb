class NotafornecedoresController < ApplicationController
  before_action :set_notafornecedor, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @notafornecedores = Notafornecedor.all
    respond_with(@notafornecedores)
  end

  def show
    respond_with(@notafornecedor)
  end

  def new
    @notafornecedor = Notafornecedor.new
    respond_with(@notafornecedor)
  end

  def edit
  end

  def create
    @notafornecedor = Notafornecedor.new(notafornecedor_params)
    @notafornecedor.save
    respond_with(@notafornecedor)
  end

  def update
    @notafornecedor.update(notafornecedor_params)
    respond_with(@notafornecedor)
  end

  def destroy
    @notafornecedor.destroy
    respond_with(@notafornecedor)
  end

  private
    def set_notafornecedor
      @notafornecedor = Notafornecedor.find(params[:id])
    end

    def notafornecedor_params
      params.require(:notafornecedor).permit(:fornecedor, :data_compra, :valor_devolvido, :valor_retirado, :total, :descricao, :fornecedor_id,
      estoque_attributes: [:id, :nome_produto, :valor, :quantidade, :porcentagemlucro, :notafornecedor_id, :_destroy])
    end
end
