class ClientesController < ApplicationController
  before_action :set_cliente, only: [:show, :venda, :edit, :update, :destroy]

  respond_to :html

  # GET /clientes
  # GET /clientes.json
  def index
    #@clientes = Cliente.all
    @clientes = Cliente.order('nome').paginate(:page => params[:page], :per_page => 30)
    @clientes_total = Cliente.all.count
    # @clientes = Cliente.order(:nome).page params[:page]
  end

  # GET /clientes/1
  # GET /clientes/1.json
  def show
    @pontual = Movimento.pontual(@cliente)
    @atrasada = Movimento.atrasada(@cliente)
    @fidelidade = Configuracao.first
  end

  def venda

  end

  def agendamento
    @clienteagenda = Cliente.where(cpf: params[:search])
    # if @clientes
    #   redirect_to new_cliente_path
    # else
    #   redirect_to @cliente.agendamento
    # end
  end

  def ultimoagendamento
    # @clientes = Cliente.all
  end

  def busca
    @busca_quem = params[:search]
    @clientebusca = Cliente.search_cliente(@busca_quem)
  end

  def fidelidade_busca
    @busca = params[:cliente]
    @clientebusca = Cliente.where(nome: @busca)
  end

  # GET /clientes/new
  def new
    @cliente = Cliente.new
  end

  # GET /clientes/1/edit
  def edit

  end

  # POST /clientes
  # POST /clientes.json
  def create
    @cliente = Cliente.new(cliente_params)
    if !Cliente.nomeduplicado(@cliente.nome).present?
      @cliente.save
      respond_with(@cliente)
    else
      respond_to.html { redirect_to new_cliente_path, notice: 'Nome já cadastrado no sistema.' }
    end
  end

  # PATCH/PUT /clientes/1
  # PATCH/PUT /clientes/1.json
  def update
    @cliente.update(cliente_params)
    respond_with(@cliente, location: cliente_path)
  end

  # DELETE /clientes/1
  # DELETE /clientes/1.json
  def destroy
    @cliente.destroy
    respond_to do |format|
      format.html { redirect_to clientes_url, notice: 'Cliente excluido com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_cliente
    @cliente = Cliente.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def cliente_params
    params.require(:cliente).permit(:nome, :email, :telefone, :cpf, :observacao, :cidade, :profissao, :uf, :data_nascimento, :endereco, :bairro, :estoque_ids,
    vendas_attributes: [:id, :cliente_id, :estoque_id, :quantidade, :_destroy])
  end
end
