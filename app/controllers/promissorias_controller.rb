class PromissoriasController < ApplicationController
  before_action :set_promissoria, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @promissorias = Promissoria.order('id DESC').paginate(:page => params[:page], :per_page => 30)
    respond_with(@promissorias)
  end

  def show
    respond_with(@promissoria)
  end

  def new
    @promissoria = Promissoria.new
    @promissoria.cliente_id = params[:cliente_id]
    @promissoria.venda_id = params[:venda_id]
    if @promissoria.venda_id.present?
      @promissoria.valor_debito = Venda.find(params[:venda_id]).valor_total
    end
    respond_with(@promissoria)
  end

  def edit
  end

  def create
    @promissoria = Promissoria.new(promissoria_params)
    @promissoria.save
    respond_with(@promissoria, location: cliente_path(@promissoria.cliente_id))
  end

  def update
    @promissoria.update(promissoria_params)
    respond_with(@promissoria, location: cliente_path(@promissoria.cliente_id))
  end

  def destroy
    @promissoria.destroy
    respond_with(@promissoria)
  end

  private
    def set_promissoria
      @promissoria = Promissoria.find(params[:id])
    end

    def promissoria_params
      params.require(:promissoria).permit(:descricao, :data_vencimento, :valor_debito, :cliente_id)
    end
end
