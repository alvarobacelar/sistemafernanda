class ConfiguracoesController < ApplicationController
  before_action :set_configuracao, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @configuracoes = Configuracao.all
    respond_with(@configuracoes)
  end

  def show
    respond_with(@configuracao)
  end

  def new
    @configuracao = Configuracao.new
    respond_with(@configuracao)
  end

  def edit
  end

  def create
    @configuracao = Configuracao.new(configuracao_params)
    @configuracao.save
    respond_with(@configuracao)
  end

  def update
    @configuracao.update(configuracao_params)
    respond_with(@configuracao)
  end

  def destroy
    @configuracao.destroy
    respond_with(@configuracao)
  end

  private
    def set_configuracao
      @configuracao = Configuracao.find(params[:id])
    end

    def configuracao_params
      params.require(:configuracao).permit(:fidelidade_valor)
    end
end
