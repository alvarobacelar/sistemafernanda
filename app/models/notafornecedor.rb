class Notafornecedor < ApplicationRecord

  validates :valor_retirado, :valor_devolvido,  presence: true

  # usar_como_dinheiro :valor_devolvido
  # usar_como_dinheiro :valor_retirado
  # usar_como_dinheiro :total

  belongs_to :fornecedor, optional: true

  has_many :estoque
  accepts_nested_attributes_for :estoque, :allow_destroy => true

  before_save :soma_total
  def soma_total
      self.total = self.valor_retirado - self.valor_devolvido
  end

end
