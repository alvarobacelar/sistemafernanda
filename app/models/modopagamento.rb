class Modopagamento < ApplicationRecord

  has_many :movimento

  scope :tipopagamentografico, -> { joins(:movimento) }

end
