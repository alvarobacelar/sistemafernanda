class Cliente < ApplicationRecord

  validates :nome, :cidade, :uf, :telefone, :bairro, presence: true
  # validates :email, format: /[a-z|0-9]+@[a-z|0-9]+.[a-z]/

  has_many :movimento
  has_many :promissorias
  has_many :compras
  has_many :vendas
  has_many :fidelidade

  data = Date.today
  aniv = data.strftime('%d/%m')
  data_inicio = data.beginning_of_week
  data_fim = data.end_of_week

  scope :search_cliente, -> (nome) { where("unaccent(nome) ilike ?", "%#{nome}%") }

  semana = data_inicio.cweek

  scope :nomeduplicado, -> (nome) { where(nome: nome) }
  scope :aniversario_semana, -> { where(nascimento: semana) }

  scope :busca_cliente_fidelidade, -> (cliente) { joins(:fidelidade).where(nome: cliente) }


  self.per_page = 30
  #max_paginates_per 100
  def receitacliente
    @movimento = Movimento.find(cliente_id: @cliente)
  end

  def self.aniversariantes_do_dia(data_referencia = Date.today)
    where("to_char(data_nascimento, 'DD/MM') = ?", data_referencia.strftime('%d/%m')).
    sort_by { |a| a.data_nascimento.day }
  end

  def self.aniversariantes_da_semana(data_referencia = Date.today)
    inicio_semana = data_referencia.beginning_of_week.strftime('%m%d').to_i
    fim_semana = data_referencia.end_of_week.strftime('%m%d').to_i
    where("data_nascimento is not null and to_char(data_nascimento, 'MMDD')::int between ? and ?", inicio_semana, fim_semana ).
    sort_by { |a| a.data_nascimento.day }
  end

end
