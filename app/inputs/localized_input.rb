class LocalizedInput < SimpleForm::Inputs::Base
  enable :placeholder

  def input(wrapper_options = nil)
    #merged_input_options = merge_wrapper_options(input_html_options, wrapper_options)
    #@builder.text_field(attribute_name, merged_input_options)
    @builder.text_field(attribute_name, input_html_options)
  end

  def input_html_options
    input_html_options = super
    input_html_options[:value] = attribute_value if attribute_value
    input_html_options
  end

  protected

  def attribute_value
    @value ||= object.send(attribute_name)
  end
end
