module ApplicationHelper
  def app_name
    "Fernanda Boutique"
  end

  def tema_app
    "teal"
  end

  def logo
    "logo.png"
  end

  def logo_width
    nil
  end

  def logo_header
    nil
  end

  def logo_header_width
    '252px'
  end

  def background_header
    'inherit'
  end

  def link_color
    '#428bca'
  end
end
