module PromissoriasHelper

  def conta_data(data)
    hoje = Date.today
    @quant_dias = (data - hoje).to_i
    if @quant_dias <= 5 && @quant_dias >= 1
      "Promissoria proxima de vencer (#{@quant_dias} dias)"
    elsif @quant_dias == 0
      "Promissoria vencendo hoje"
    elsif @quant_dias < 0
      "Promissoria VENCIDA"
    else
      "Faltam #{@quant_dias} dias"
    end
  end

  def class_vencimento(data)
    hoje = Date.today
    @quant_dias = (data - hoje).to_i
    if @quant_dias <= 5 && @quant_dias >= 1
      "text-info"
    elsif @quant_dias == 0
      "text-warning"
    elsif @quant_dias < 0
      "text-danger"
    else
      ""
    end
  end

  def ver_promissorias
    params[:order] = 'created_at'
    @cliente.vendas.limit(20).order("#{params[:order]} DESC")
  end

end
