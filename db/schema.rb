# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170527121522) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "unaccent"

  create_table "categoriamovimentos", force: :cascade do |t|
    t.string   "nome"
    t.text     "descricao"
    t.integer  "movimento_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "clientes", force: :cascade do |t|
    t.string   "nome"
    t.string   "email"
    t.string   "telefone"
    t.string   "cpf"
    t.string   "observacao"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "nascimento"
    t.text     "endereco"
    t.string   "bairro"
    t.string   "cidade"
    t.string   "uf"
    t.date     "data_nascimento"
  end

  create_table "compras", force: :cascade do |t|
    t.decimal  "valor_total"
    t.string   "status"
    t.integer  "venda_id"
    t.integer  "cliente_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "quantidade"
    t.integer  "estoque_id"
  end

  create_table "configuracoes", force: :cascade do |t|
    t.integer  "fidelidade_valor"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "estoques", force: :cascade do |t|
    t.string   "nome_produto"
    t.text     "descricao"
    t.decimal  "valor"
    t.integer  "quantidade"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "notafornecedor_id"
    t.decimal  "porcentagemlucro"
    t.decimal  "valordevenda"
    t.decimal  "lucro"
    t.decimal  "totalproduto"
  end

  create_table "fidelidades", force: :cascade do |t|
    t.integer  "quant_compra"
    t.integer  "cliente_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.boolean  "status"
  end

  create_table "fornecedores", force: :cascade do |t|
    t.string   "nome"
    t.string   "cnpj"
    t.string   "telefone"
    t.text     "endereco"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lembretes", force: :cascade do |t|
    t.text     "texto"
    t.date     "datadealerta"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "modopagamentos", force: :cascade do |t|
    t.string   "tipo"
    t.decimal  "porcentagem"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "movimentos", force: :cascade do |t|
    t.decimal  "valor_previsto"
    t.date     "data_prevista"
    t.decimal  "valor_realizado"
    t.date     "data_realizado"
    t.text     "descricao"
    t.string   "tipo"
    t.integer  "cliente_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.decimal  "desconto"
    t.string   "modopagamento"
    t.string   "categoriamovimento_id"
    t.integer  "modopagamento_id"
    t.decimal  "valor_dinheiro"
    t.decimal  "valor_cartao"
    t.integer  "venda_id"
    t.boolean  "fidelidade"
  end

  create_table "notafornecedores", force: :cascade do |t|
    t.string   "fornecedor"
    t.date     "data_compra"
    t.decimal  "valor_devolvido"
    t.decimal  "valor_retirado"
    t.decimal  "total"
    t.text     "descricao"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "fornecedor_id"
  end

  create_table "pagamentos", force: :cascade do |t|
    t.integer  "movimento_id"
    t.integer  "promissoria_id"
    t.decimal  "valor_devedor"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "promissoria", force: :cascade do |t|
    t.text     "descricao"
    t.decimal  "valor_total"
    t.date     "data_compra"
    t.date     "data_vencimento"
    t.integer  "cliente_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "promissorias", force: :cascade do |t|
    t.string   "descricao"
    t.date     "data_vencimento"
    t.decimal  "valor_debito"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "cliente_id"
    t.integer  "movimento_id"
    t.integer  "venda_id"
    t.decimal  "valor_restante"
  end

  create_table "usuarios", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "nome"
    t.string   "perfil"
    t.boolean  "ativo"
    t.index ["email"], name: "index_usuarios_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_usuarios_on_reset_password_token", unique: true, using: :btree
  end

  create_table "vendas", force: :cascade do |t|
    t.date     "data_venda"
    t.decimal  "valor_total"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "cliente_id"
    t.text     "descricao"
  end

end
