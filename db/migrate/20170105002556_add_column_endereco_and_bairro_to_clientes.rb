class AddColumnEnderecoAndBairroToClientes < ActiveRecord::Migration[5.0]
  def change
    add_column :clientes, :endereco, :text
    add_column :clientes, :bairro, :string
  end
end
