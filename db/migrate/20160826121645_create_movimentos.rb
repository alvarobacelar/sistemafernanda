class CreateMovimentos < ActiveRecord::Migration[5.0]
  def change
    create_table :movimentos do |t|
      t.decimal :valor_previsto
      t.date :data_prevista
      t.decimal :valor_realizado
      t.date :data_realizado
      t.text :descricao
      t.string :tipo
      t.integer :cliente_id

      t.timestamps
    end
  end
end
