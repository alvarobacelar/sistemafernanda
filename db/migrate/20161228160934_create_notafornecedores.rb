class CreateNotafornecedores < ActiveRecord::Migration[5.0]
  def change
    create_table :notafornecedores do |t|
      t.string :fornecedor
      t.date :data_compra
      t.decimal :valor_devolvido
      t.decimal :valor_retirado
      t.decimal :total
      t.text :descricao

      t.timestamps
    end
  end
end
