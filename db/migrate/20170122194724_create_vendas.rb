class CreateVendas < ActiveRecord::Migration[5.0]
  def change
    create_table :vendas do |t|
      t.date :data_venda
      t.numeric :valor_total

      t.timestamps
    end
  end
end
