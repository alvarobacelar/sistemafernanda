class AddColumnValordevendaToEstoques < ActiveRecord::Migration[5.0]
  def change
    add_column :estoques, :valordevenda, :numeric
  end
end
