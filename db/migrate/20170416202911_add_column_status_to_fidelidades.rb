class AddColumnStatusToFidelidades < ActiveRecord::Migration[5.0]
  def change
    add_column :fidelidades, :status, :boolean
  end
end
