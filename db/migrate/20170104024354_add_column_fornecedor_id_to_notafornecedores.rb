class AddColumnFornecedorIdToNotafornecedores < ActiveRecord::Migration[5.0]
  def change
    add_column :notafornecedores, :fornecedor_id, :integer
  end
end
