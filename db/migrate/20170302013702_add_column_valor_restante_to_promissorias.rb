class AddColumnValorRestanteToPromissorias < ActiveRecord::Migration[5.0]
  def change
    add_column :promissorias, :valor_restante, :decimal
  end
end
