class AddColumnMovimentoIdToPromissorias < ActiveRecord::Migration[5.0]
  def change
    add_column :promissorias, :movimento_id, :integer
  end
end
