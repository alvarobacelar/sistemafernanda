class CreatePromissorias < ActiveRecord::Migration[5.0]
  def change
    create_table :promissorias do |t|
      t.string :descricao
      t.date :data_vencimento
      t.decimal :valor_debito

      t.timestamps
    end
  end
end
