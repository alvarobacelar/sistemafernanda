class AddColumnTotalprodutoToEstoques < ActiveRecord::Migration[5.0]
  def change
    add_column :estoques, :totalproduto, :decimal
  end
end
