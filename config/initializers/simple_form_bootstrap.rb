inputs = %w[
  CollectionSelectInput
  DateTimeInput
  FileInput
  GroupedCollectionSelectInput
  NumericInput
  PasswordInput
  RangeInput
  StringInput
  TextInput
]


inputs.each do |input_type|
  superclass = "SimpleForm::Inputs::#{input_type}".constantize

  new_class = Class.new(superclass) do
    def input_html_classes
      super.push('form-control')
    end
  end

  Object.const_set(input_type, new_class)
end

# Use this setup block to configure all options available in SimpleForm.
SimpleForm.setup do |config|
  # config.error_notification_class = 'alert alert-danger'
  # config.button_class = 'btn btn-default'
  # config.boolean_label_class = nil

  config.wrappers :vertical_form, tag: 'div', class: 'form-group fg-float', error_class: 'has-error has-feedback' do |b|
    b.use :html5
    b.wrapper tag: 'div', class: 'fg-line' do |ba|
      ba.use :placeholder
      ba.optional :maxlength
      ba.optional :pattern
      ba.optional :min_max
      ba.optional :readonly
      ba.use :label, class: 'fg-label'

      ba.use :input, class: 'form-control fg-input'
      ba.use :error, wrap_with: { tag: 'span', class: 'help-block' }
      ba.use :hint,  wrap_with: { tag: 'p', class: 'help-block' }
    end
  end

  config.wrappers :material, tag: 'div', class: 'form-group fg-float', error_class: 'has-error' do |b|
    b.wrapper :tag => 'div', :class => 'fg-line' do |ba|
      ba.use :placeholder
      ba.use :label
      ba.use :input
    end
    b.use :error, wrap_with: { tag: 'small', class: 'help-block' }
    b.use :hint,  wrap_with: { tag: 'p', class: 'help-inline' }
  end

  config.wrappers :select_for, tag: 'div', class: 'col-sm-6 m-b-25', error_class: 'has-error has-feedback' do |b|
    b.use :html5
    b.use :placeholder
    b.optional :maxlength
    b.optional :pattern
    b.optional :min_max
    b.optional :readonly
    b.optional :label
    b.use :input
    b.use :error, wrap_with: { tag: 'span', class: 'help-block' }
    b.use :hint,  wrap_with: { tag: 'p', class: 'help-block' }
  end

  config.wrappers :vertical_file_input, tag: 'div', class: 'form-group fg-float', error_class: 'has-error has-feedback' do |b|
    b.use :html5
    b.wrapper tag: 'div', class: 'fg-line' do |ba|
      ba.use :placeholder
      ba.optional :maxlength
      ba.optional :readonly
      ba.use :label, class: 'fg-label'

      ba.use :input
      ba.use :error, wrap_with: { tag: 'span', class: 'help-block' }
      ba.use :hint,  wrap_with: { tag: 'p', class: 'help-block' }
    end
  end

  config.wrappers :vertical_boolean, tag: 'div', class: 'form-group', error_class: 'has-error has-feedback' do |b|
    b.use :html5
    b.optional :readonly
    b.wrapper tag: 'div', class: 'checkbox' do |ba|
      ba.use :label_input
    end

    b.use :error, wrap_with: { tag: 'span', class: 'help-block' }
    b.use :hint,  wrap_with: { tag: 'p', class: 'help-block' }
  end

  config.wrappers :vertical_radio_and_checkboxes, tag: 'div', class: 'form-group', error_class: 'has-error has-feedback' do |b|
    b.use :html5
    b.optional :readonly
    b.use :label, class: 'control-label'
    b.use :input
    b.use :error, wrap_with: { tag: 'span', class: 'help-block' }
    b.use :hint,  wrap_with: { tag: 'p', class: 'help-block' }
  end

  config.wrappers :horizontal_form, tag: 'div', class: 'form-group fg-float', error_class: 'has-error has-feedback' do |b|
    b.use :html5
    b.wrapper tag: 'div', class: 'fg-line' do |ba|
      ba.use :placeholder
      ba.optional :maxlength
      ba.optional :pattern
      ba.optional :min_max
      ba.optional :readonly
      ba.use :label, class: 'fg-label'

      ba.use :input, class: 'form-control fg-input'
      ba.use :error, wrap_with: { tag: 'span', class: 'help-block' }
      ba.use :hint,  wrap_with: { tag: 'p', class: 'help-block' }
    end
  end

  config.wrappers :date_time, tag: 'div', class: 'form-group fg-float', error_class: 'has-error has-feedback' do |b|
    b.use :html5
    b.wrapper tag: 'div', class: 'fg-line' do |ba|
      ba.use :placeholder
      ba.optional :maxlength
      ba.optional :pattern
      ba.optional :min_max
      ba.optional :readonly
      ba.use :label, class: 'fg-label'

      ba.use :input, class: 'form-control date-time-picker'
      ba.use :error, wrap_with: { tag: 'span', class: 'help-block' }
      ba.use :hint,  wrap_with: { tag: 'p', class: 'help-block' }
    end
  end

  config.wrappers :date_only, tag: 'div', class: 'form-group fg-float', error_class: 'has-error has-feedback' do |b|
    b.use :html5
    b.wrapper tag: 'div', class: 'fg-line' do |ba|
      ba.use :placeholder
      ba.optional :maxlength
      ba.optional :pattern
      ba.optional :min_max
      ba.optional :readonly
      ba.use :label, class: 'fg-label'

      ba.use :input, class: 'form-control date-picker'
      ba.use :error, wrap_with: { tag: 'span', class: 'help-block' }
      ba.use :hint,  wrap_with: { tag: 'p', class: 'help-block' }
    end
  end

  config.wrappers :horizontal_file_input, tag: 'div', class: 'form-group', error_class: 'has-error has-feedback' do |b|
    b.use :html5
    b.use :placeholder
    b.optional :maxlength
    b.optional :readonly
    b.use :label, class: 'col-sm-3 control-label'

    b.wrapper tag: 'div', class: 'col-sm-9' do |ba|
      ba.use :input
      ba.use :error, wrap_with: { tag: 'span', class: 'help-block' }
      ba.use :hint,  wrap_with: { tag: 'p', class: 'help-block' }
    end
  end

  config.wrappers :horizontal_radio_and_checkboxes, tag: 'div', class: 'form-group', error_class: 'has-error has-feedback' do |b|
    b.use :html5
    b.optional :readonly

    b.use :label, class: 'col-sm-3 control-label'

    b.wrapper tag: 'div', class: 'col-sm-9' do |ba|
      ba.use :input
      ba.use :error, wrap_with: { tag: 'span', class: 'help-block' }
      ba.use :hint,  wrap_with: { tag: 'p', class: 'help-block' }
    end
  end

  config.wrappers :inline_form, tag: 'div', class: 'form-group', error_class: 'has-error has-feedback' do |b|
    b.use :html5
    b.use :placeholder
    b.optional :maxlength
    b.optional :pattern
    b.optional :min_max
    b.optional :readonly
    b.use :label, class: 'sr-only'

    b.use :input, class: 'form-control'
    b.use :error, wrap_with: { tag: 'span', class: 'help-block' }
    b.use :hint,  wrap_with: { tag: 'p', class: 'help-block' }
  end

  config.wrappers :multi_select, tag: 'div', class: 'form-group', error_class: 'has-error has-feedback' do |b|
    b.use :html5
    b.optional :readonly
    b.use :label, class: 'control-label'
    b.wrapper tag: 'div', class: 'form-inline' do |ba|
      ba.use :input, class: 'form-control'
      ba.use :error, wrap_with: { tag: 'span', class: 'help-block' }
      ba.use :hint,  wrap_with: { tag: 'p', class: 'help-block' }
    end
  end
  # Wrappers for forms and inputs using the Bootstrap toolkit.
  # Check the Bootstrap docs (http://getbootstrap.com)
  # to learn about the different styles for forms and inputs,
  # buttons and other elements.
  config.default_wrapper = :vertical_form

end
